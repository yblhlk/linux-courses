//未来在公司日志是必须要写的
#pragma once

#include <iostream>
#include <string>
#include <cstdio>
#include <ctime>
#include <cstring>
//stdarg.h是C语言中C标准函数库的头文件，
//stdarg是由standard（标准） arguments（参数）简化而来，
//主要目的为让函数能够接收可变参数。
#include <stdarg.h>

//日志是有日志级别的
#define DEBUG   0 //调试信息（打印提示信息使用该级别）
#define NORMAL  1 //正常
#define WARNING 2 //警告
#define ERROR   3 //错误
#define FATAL   4 //致命错误

//等级和日志信息的映射表
const char *gLevelMap[] = {
    "DEBUG", "NORMAL",
    "WARNING", "ERROR",
    "FATAL"
};

//完整的日志功能至少有：
//日志等级、日期时间、支持用户自定义(可变参数实现)
void logMessage(int level, const char*format, ...)
{
#ifndef DEBUG_SHOW //如果没有调试宏，遇到DEBUG的log跳过。
    if(level == DEBUG) return;
#endif
    //1.日志的标准部分：日志等级、日期时间
    char stdBuffer[1024] = {0};

    time_t timestamp = time(nullptr); //获取时间戳
    struct tm* timeinfo; //保存时间信息的结构体
    char tmbuffer[128] = {0}; //保存数据信息的字符串
    timeinfo = localtime(&timestamp); // 用时间戳计算出时间结构体的参数
    strftime(tmbuffer,sizeof(tmbuffer),"Now is %Y/%m/%d %H:%M:%S",timeinfo);

    int len = snprintf(stdBuffer, sizeof stdBuffer, "[%s][%s]", gLevelMap[level], tmbuffer);
    stdBuffer[len] = '\0';

    //2.日志的用户自定义部分
    char logBuffer[1024] = {0};
    va_list args;
    va_start(args, format);
    vsnprintf(logBuffer, sizeof logBuffer, format, args);
    va_end(args);

    //这是打印到显示器的版本，如果要打印到文件使用fprintf
    printf("%s %s", stdBuffer, logBuffer);
}