#include "server.hpp"
#include "log.hpp"
#include <memory> //智能指针
#include <cstdlib> //exit()

//argc 是 argument count的缩写,表示argv这个二级指针指向的内存区域中保存的由stub写入的有效命令行参数的个数.
//argv 是 argument vector的缩写,表示传入main函数的参数序列或指针,并且第一个参数argv[0]一定是程序的名称,
int main(int argc, char* argv[])
{
    if(argc != 2) //如果传入的参数不满足3个的要求。
    {
        logMessage(FATAL, "Usage: %s need: proc.\n", argv[0]);
        exit(1);
    }
    //从命令行参数里获取：IP地址
    //string ip = argv[1];
    //从命令行参数里获取：获取端口号
    uint16_t port = atoi(argv[1]); //atoi():ASCII to int 把字符串(const char*)转换成整型数，stoi()把字符串(string)转化为整数

    //创建服务器对象
    std::unique_ptr<UdpServer> svr(new UdpServer(port));//使用智能指针，让智能制造对后续的对象进行管理。
    //初始化服务器
    svr->initServer();
    //启动服务器
    svr->Start();

    //使用delete释放new出来的服务器对象。
    //delete svr;//我们这里使用了智能制造，所以不需要使用delete来释放对象了
    return 0;
}