#include "threadpool.hpp"
#include "Task.hpp"
#include <ctime>

void add(int x, int y)
{
    cout << x << " + " << y << " = " << x+y << endl;
}
void sub(int x, int y)
{
    cout << x << " - " << y << " = " << x-y << endl;
}
void mux(int x, int y)
{
    cout << x << " * " << y << " = " << x*y << endl;
}
void del(int x, int y) //地板除
{
    if(y)
        cout << x << " // " << y << " = " << x/y << endl;
    else
        cout << x << " // " << y << "，y为0无法进行除法！" << endl;

}

//主线程是生产者，负责生产任务，并上传任务到线程池的任务队列中
int main()
{
    //1.创建并启动线程池
    //单例模式下无法直接new一个线程池对象了
    //ThreadPool<Task> *tp = new ThreadPool<Task>();
    //调用静态方法创建一个单例模式的对象
    ThreadPool<Task> *tp = ThreadPool<Task>::getThreadPool(6);//调用一个类内的静态函数：类名::静态成员函数名

    //2.产生任务，并上传任务到线程池的任务队列中
    //2.1 生产任务
    srand((unsigned int)time(nullptr) ^ getpid() ^ 6132);
    func_t func[4] = {add, sub, mux, del};
    char flag[4] = {'+', '-', '*', '/'};
    while(1)
    {
        int x = rand()%10+1;
        usleep(4564);
        int y = rand()%10+1;
        int f = rand()%4; //产生0~3之间的数。
        Task t(x, y, func[f]);
        cout << "任务: " << x << flag[f] << y << "=? 制作完成。" << endl;

        //2.2推送任务
        tp->pushTask(t);

        sleep(1);
    }
    
    //3.等待线程退出（防止出现子线程未执行完，主线程已退出的情况）
    tp->tp_jion();
    //4.释放线程池
    delete tp;
    return 0;
}
