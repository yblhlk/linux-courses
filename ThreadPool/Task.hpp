//如果生产者消费者模型不是产生和消费数据，而是生成和接受任务呢？
//派发任务式的生产者消费者模型，应用：生成问题-抢答（单生产，多消费）; 任务分配系统（单生产，多消费）。
#pragma once

#include<iostream>
#include<functional>

typedef std::function<void(int, int)> func_t;

class Task
{
private:
    int x_;
    int y_;
    func_t func_;
public:
    Task(){}
    Task(int x, int y, func_t func):x_(x), y_(y), func_(func)
    {}

    // 仿函数:通过重载operator()，让类可以模仿函数调用的行为： 函数名(参数)； 类名() 调用的函数行为。
    void operator()(std::string name)
    {
        std::cout << name << "，调用任务：";
        func_(x_, y_);
    }

    //获取私有成员的方法
    int getx()
    {
        return x_;
    }
    int gety()
    {
        return y_;
    }
};
 


// std::function的底层实现：
// template<typename R , typename... A >
// class MyFunction4<R(A...)> {
 
// public:
//     typedef R(*PFUNCTION)(A...);
 
//     MyFunction4<R(A...)>(PFUNCTION _p) : function(_p) {}
 
//     R operator()(A... arg) {
//         return (*function)(arg...);
//     }
 
// private:
 
//     PFUNCTION function;
// };