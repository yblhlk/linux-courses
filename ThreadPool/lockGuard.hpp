//RAII风格的加锁方式
//利用构造函数和析构函数自动完成加锁和解锁
#pragma once

#include<iostream>
#include<pthread.h>

class lockGuard
{
private:
    pthread_mutex_t* pmut_;
public:
    //用初始化列表初始化类成员
    lockGuard(pthread_mutex_t *pmut):pmut_(pmut) //利用构造函数完成加锁
    {
        pthread_mutex_lock(pmut_);
    }

    ~lockGuard() //利用析构函数完成解锁
    {
        pthread_mutex_unlock(pmut_);
    }
};


//优化前的版本：
// class Mutex 
// {
// private:
//     pthread_mutex_t* pmut_;
// public:
//     //构造函数
//     Mutex(pthread_mutex_t *pmut):pmut_(pmut){}
//     void lock()
//     {
//         pthread_mutex_lock(pmut_);
//         std::cout << "线程[" << pthread_self() << "]进行加锁" << std::endl;
//     }
//     void unlock()
//     {
//         pthread_mutex_unlock(pmut_);
//         std::cout << "线程[" << pthread_self() << "]进行解锁" << std::endl;
//     }
// };

// class lockGuard
// {
// private:
//     Mutex mut_;
// public:
//     //用初始化列表初始化类成员
//     lockGuard(pthread_mutex_t *pmut):mut_(pmut) //利用构造函数完成加锁
//     {
//         mut_.lock();
//     }

//     ~lockGuard() //利用析构函数完成解锁
//     { 
//         mut_.unlock();
//     }
// };