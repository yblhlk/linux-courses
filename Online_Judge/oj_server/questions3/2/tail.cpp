#include <sys/time.h> //struct timeval / gettimeofday()
#include <unistd.h>
#include <algorithm>
#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

int main()
{
    Solution s;
    //1. 测试用例
    std::vector<std::vector<int>> input1 = {{2,7,11,15},
                                            {3,2,4},
                                           };
    int input2[] = {9, 6};
    //2. 每个测试用例的答案
    std::vector<std::vector<int>> answer = {{0,1},
                                            {1,2},
                                           };
    //3. 用例的组数
    int len = sizeof(input2) / sizeof(input2[0]);
    //4. 计算程序执行时间
    //4.1 获取程序开始时间
    struct timeval start_time;
    gettimeofday(&start_time, nullptr);

    //开始判题
    int count = 0; //记录当前通过的用例数量
    while (count < len)
    {
        std::vector<int> re = s.twoSum(input1[count], input2[count]);
        std::vector<int> an = answer[count];
        /* C++ 算法 equal()function 比较两个容器中的元素，如果发现两个容器中的所有元素都匹配，则返回真值。
       第一个范围从 [first1,last1) 开始，第二个范围从 first2 开始。*/
        if (equal(re.begin(), re.end(), an.begin()))
        {
            std::cout << "用例" << count+1 <<": 通过。\n";
        }
        else
        {
            printf("用例%d: [", count+1);
            for(auto it : input1[count])
            {
                std::cout << it << " ";
            }
            printf("] , %d 未通过，", input2[count]);
            printf("你的结果为：[");
            for(auto it : re)
            {
                std::cout << it << " ";
            }
            printf("]，正确结果为：[");
            for(auto it : an)
            {
                std::cout << it << " ";
            }
            printf("]\n");
            break;
        }
        ++count;
    }
    //4.2 获取程序结束时间
    struct timeval end_time;
    gettimeofday(&end_time, nullptr);
    //4.3 计算程序执行时间（单位为毫秒）
    long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000 +
                       (end_time.tv_usec - start_time.tv_usec) / 1000;

    //5. 获取进程当前占用的内存（单位为 KB）
    long used_memory = sysconf(_SC_PHYS_PAGES) / 1024;
    //6. 显示通过情况：
    if(count == len)
        printf("通过全部用例，运行时间：%ld ms, 占用内存：%ldkb。\n", elapsed_time, used_memory);
    else
        printf("%d/%d组用例通过, 运行时间：%ld ms, 占用内存：%ldkb。\n", count, len, elapsed_time, used_memory);
    return 0;
}