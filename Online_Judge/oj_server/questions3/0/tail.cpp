// 对于接口型的题目，tail文件中保存头文件、接口声明、main函数;
// 对于要自己写main函数的题目tail文件里什么都不写。
// head文件中保存用户代码；
#include <sys/time.h> //struct timeval / gettimeofday()
#include <unistd.h>
#include <cstdio>
#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

int main()
{
    Solution s;
    //1. 测试用例
    int input[] = {1, 2, 
                   3, 4,
                   5, 6};

    //2. 每个测试用例的答案
    int answer[] = {3, 7, 11};
    //3. 用例的组数
    int len = sizeof(answer) / sizeof(answer[0]);
    //4. 计算程序执行时间
    //4.1 获取程序开始时间
    struct timeval start_time;
    gettimeofday(&start_time, nullptr);

    //开始判题
    int count = 0; //记录当前通过的用例数量
    while (count < len)
    {
        int re = s.add(input[count*2], input[count*2+1]);
        int an = answer[count];
        if(re == an)
        {
            std::cout << "用例" << count+1 <<": 通过。\n";
        }
        else
        {
            printf("用例%d: (%d,%d)未通过，你的结果为：%d，正确结果为：%d。\n", count+1, input[count*2], input[count*2+1], re, an);
            break;
        }
        ++count;
    }
    //4.2 获取程序结束时间
    struct timeval end_time;
    gettimeofday(&end_time, nullptr);
    //4.3 计算程序执行时间（单位为毫秒）
    long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000 +
                       (end_time.tv_usec - start_time.tv_usec) / 1000;

    //5. 获取进程当前占用的内存（单位为 KB）
    long used_memory = sysconf(_SC_PHYS_PAGES) / 1024;
    //6. 显示通过情况：
    if(count == len)
        printf("通过全部用例，运行时间：%ld ms, 占用内存：%ldkb。\n", elapsed_time, used_memory);
    else
        printf("%d/%d组用例通过, 运行时间：%ld ms, 占用内存：%ldkb。\n", count, len, elapsed_time, used_memory);
    return 0;
}