#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void Test1()
{
    vector<int> v = {2,7,11,15};
    vector<int> index = Solution().twoSum(v, 9);
    vector<int> judge = {0, 1};
    /* C++ 算法 equal()function 比较两个容器中的元素，如果发现两个容器中的所有元素都匹配，则返回真值。
       第一个范围从 [first1,last1) 开始，第二个范围从 first2 开始。*/
    if (equal(index.begin(), index.end(), judge.begin()))
    {
        std::cout << "测试用例1通过。" << std::endl;
    }
    else
    {
        std::cout << "测试用例1....失败, 测试的内容是{2,7,11,15}, 正确结果是[0 ,1]。" << std::endl;
    }
}

void Test2()
{
    vector<int> v = {3,2,4};
    vector<int> index = Solution().twoSum(v, 6);
    vector<int> judge = {1, 2};
    if (equal(index.begin(), index.end(), judge.begin()))
    {
        std::cout << "测试用例2通过" << std::endl;
    }
    else
    {
        std::cout << "测试用例2....失败, 测试的内容是{3,2,4}, 正确结果是[1 ,2]。" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}