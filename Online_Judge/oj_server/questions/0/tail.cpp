#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void Test1()
{
    int add = Solution().add(1, 2);
    if (add == 3)
    {
        std::cout << "测试用例1通过。" << std::endl;
    }
    else
    {
        std::cout << "测试用例1....失败。" << std::endl;
    }
}

void Test2()
{
    int add = Solution().add(5, 6);
    if (add == 11)
    {
        std::cout << "测试用例2通过。" << std::endl;
    }
    else
    {
        std::cout << "测试用例2....失败。" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}