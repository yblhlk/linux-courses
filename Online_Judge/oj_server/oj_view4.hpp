//渲染网页模块：使用ctemplate库渲染网页
#pragma once

#include <iostream>
#include <string>               // std::string
#include <ctemplate/template.h> // 调用ctemplate库用来渲染网页

//#include "oj_model1.hpp"        // 调用文件/数据库交互模块，获取描述问题的Question结构体
//#include "oj_model2.hpp"        //引入数据库版数据交互模块
//#include "oj_model4.hpp"          //question4题库（竞赛&&语言入门）交互模块
#include "oj_model5.hpp"          //question4题库（竞赛&&语言入门）（数据库版）交互模块

namespace ns_view4
{
    using namespace ns_model4;   // 展开文件/数据库交互模块

    const std::string template_path = "./template_html/"; //网页模板目录路径

    class View4
    {
    public:
        View4(){}
        ~View4(){}
    public:
        /*********************************************************************
         * 渲染题目列表网页
         * 参数：
         *     保存Question结构体的vector容器，里面有所有题目的信息，靠上层传入。
         *     输出型参数，给上层返回渲染好的网页。
         *********************************************************************/
        void AllExpandHtml4(const vector<struct Question4> &questions, std::string *html)
        {
            // 1. 形成要渲染的网页的路径
            std::string src_html = template_path + "all_questions4.html";
            // 2. 形成数据字典
            ctemplate::TemplateDictionary root("all_questions4"); //创建一个名为all_questions4的数据字典
            for (const auto& q : questions)
            {
                //在网页里循环生成内容，靠创建一个子数据字典：
                ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("question_list4");//创建子数据字典
                sub->SetValue("number", q.number); // 设置数据字典内容
                sub->SetValue("title", q.title);
                sub->SetValue("star", q.star);
            }

            // 3. 获取要被渲染的html文件
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP/*保持网页原貌*/);

            // 4. 开始渲染（通过输出型参数，将渲染好的网页返回给上层。）
            tpl->Expand(html, &root);
        }

        /********************************************************************
         * 渲染单个题目的网页
         * 参数：
         *     保存题目信息的Question结构体，里面有单个题目的所有信息，靠上层传入。
         *     输出型参数，给上层返回渲染好的网页。
         ********************************************************************/
        void OneExpandHtml4(const struct Question4 &q, std::string *html)
        {
            // 1. 形成要渲染的网页的路径
            std::string src_html = template_path + "one_question4.html";

            // 2. 形成数据字典
            ctemplate::TemplateDictionary root("one_question4"); //创建数据字典
            root.SetValue("number", q.number); // 设置数据字典内容
            root.SetValue("title", q.title);
            root.SetValue("star", q.star);
            root.SetValue("desc", q.desc);
            root.SetValue("pre_code", q.header);

            // 3. 获取要被渲染的html文件
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);
           
            // 4. 开始渲染（第一个参数为输出型参数，用来接收被渲染好的网页。）
            tpl->Expand(html, &root);
        }
    };
}