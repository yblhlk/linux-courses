// 在 C++ 中，要计算整个程序占用的内存，需要考虑到程序中动态分配的内存和栈上分配的变量所占用的内存。
// 由于 C++ 没有直接提供计算整个程序占用的内存的函数或方法，因此需要借助操作系统提供的系统调用或者第三方库来实现。
// 以下是一个示例代码，演示如何使用操作系统提供的系统调用计算整个程序占用的内存：

#include <iostream>
#include <sys/time.h>
#include <unistd.h>

int main() {
    // 1.获取程序开始时间
    struct timeval start_time;
    gettimeofday(&start_time, nullptr);

    // 执行程序代码
    // ...

    // 2.获取程序结束时间
    struct timeval end_time;
    gettimeofday(&end_time, nullptr);

    // 3.计算程序执行时间（单位为毫秒）
    long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000 +
                       (end_time.tv_usec - start_time.tv_usec) / 1000;

    // 4.获取进程当前占用的内存（单位为 KB）
    long used_memory = sysconf(_SC_PHYS_PAGES) / 1024;

    std::cout << "程序执行时间：" << elapsed_time << " 毫秒" << std::endl;
    std::cout << "已使用的内存：" << used_memory << " KB" << std::endl;

    return 0;
}
// 在上述代码中，我们使用了操作系统提供的 gettimeofday() 函数获取程序开始和结束的时间，并计算出程序执行的时间。
// 然后，我们使用 sysconf() 函数获取系统总内存、已使用的内存和剩余的内存，并将结果输出到控制台。
// 需要注意的是，上述代码中使用的是操作系统提供的系统调用，因此可能无法跨平台使用。
// 如果需要在不同的操作系统上获取程序占用的内存，需要使用相应操作系统提供的系统调用或第三方库。