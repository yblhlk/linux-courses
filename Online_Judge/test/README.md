test : 测试目录，包含测试 库函数 或 第三方库 的测试代码。

1. setrlimit_test.cc   : 资源限制函数setrlimit()的测试代码，用来限制程序运行时占用的资源。
2. json_test.cc        : 第三方开源库cpp-httplib的测试代码，用来完成前端发来的网络请求。
3. cpp-httplib_test.cc : 第三方开源库jsoncpp库的测试代码，用来序列化和反序列化数据以进行网络通信。
4. ctemplate_test.cc   : 第三方开源库ctemplate的测试代码，用来渲染前端网页。
5. boost_test.cc       : 第三方开源库boost中split字符串切割函数的测试代码。
6. mysql_test.cc       : mysql库Connector/C的测试代码，用来连接和操纵数据库。