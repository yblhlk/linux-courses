// 以下为第三方开源库boost中split字符串切割函数的测试代码
#include <iostream>
#include <vector>
#include <boost/algorithm/string.hpp> //boost::split() : 切割字符串

int main()
{
    std::vector<std::string> tokens;                         // 用来接收切分后的字符串内容
    const std::string str = "1:判断回文数:::::简单:1:30000";  // 要切分的字符串
    const std::string sep = ":";                             // 分隔符

    /*****************************************************************************
     * boost::split(vector, str, sep, compress)
     * 说明：按照指定分隔符切分字符串，将分隔出的内容保存到传入的vector<string>容器中。
     * 参数：
     *     vector   : 用来接收切分后的字符串内容
     *     str      : 要切分的字符串
     *     sep      : 分隔符 (使用boost::is_any_of(sep)传入)
     *     compress : 是否压缩（token_compress_on:压缩，token_compress_off:不压缩）
     *****************************************************************************/
    boost::split(tokens, str, boost::is_any_of(sep), boost::algorithm::token_compress_off);

    for (auto &iter : tokens)
    {
        std::cout << iter << std::endl;
    }
    return 0;
}


// boost::timer是Boost库提供的一个计时器类，可以用来测量代码执行的时间。它提供了一些方法和函数，可以帮助我们度量代码的执行时间。
// 以下是一个使用boost::timer的示例代码：

#include <boost/timer.hpp>

int main() {
    boost::timer timer;

    // 执行需要计时的代码
    for (int i = 0; i < 1000000; i++) {
        // do something
    }

    double elapsed_time = timer.elapsed(); // 获取代码执行时间（单位为秒）
    std::cout << "代码执行时间： " << elapsed_time << " 秒" << std::endl;

    return 0;
}