// 以下为第三方开源库cpp-httplib的测试代码
#include <iostream>
#include "../common/httplib.h" //引入第三方开源库：httplib.h 来负责网络服务（这里可以自己写一个TCP通信模块来代替）

using namespace httplib; // 展开httplib库

//作为服务端要实例化一个Server对象，来处理用户请求
int main()
{
    //1.实例化一个Server对象，Server对象中有一个请求与处理路由表，记录各种请求对应的处理函数。
    Server svr;
    //2.注册请求路由，告诉httplib，哪个请求应该用哪个函数处理。
    //对用户的hi/r1/r2请求使用lambda函数处理
    /******************************************************************************************************
     * lambda函数参数：
     * [&ctrl](const Request &req, Response &resp){}
     *     const Request &req： 请求类,里面保存着用户的请求(matches)，即/question/(\d+)和网页发来的数据(body)
     *     const Request &resq：响应类,对用户请求做出的响应
     ******************************************************************************************************/
    svr.Get("/hi/r1/r2", [](const Request &req/*请求类*/, Response &res/*响应类*/){
        std::string r1 = req.matches[0];  //获取请求的第一部分内容：hi
        std::string r2 = req.matches[1];  //获取请求的第二部分内容：r1
        std::string r3 = req.matches[2];  //获取请求的第三部分内容：r2
        res.set_content("Hello World!" + r1 + r2 + r3, "text/plain;charset=utf-8");
    });/*text/plain:文本服务；charset=utf-8:编码类型=utf-8*/

    //3.搭建服务器，开始监听。
    svr.listen("0.0.0.0", 8080);

    //4.注意更改云服务器的防火墙，不然其他主机无法访问云服务器。
    return 0;
}

//作为客户端要实例化一个Client对象，给服务端发送请求
int main()
{
    std::string ip = "127.0.0.1"; // 主机地址（127系列IP是本地回环）
    int port       = 8080;        // 端口号
    Client cli(ip, port); // 传入服务端主机IP的地址和端口号实例化一个Client对象，作为客户端
    /************************************************
     * httplib::Result Post(path, content, typ)
     * 参数 :
     *     path    : 要发起的请求
     *     content : 要传输的内容
     *     type    : 传输内容的数据类型
     * 返回值 : 
     *     返回值.body 里面是服务端返回的内容。
     ************************************************/
    if(auto res = cli.Post("/compile_and_run", "要传输的信息", "application/json;charset=utf-8"))
    {
        // 获取返回结果
        if(res->status == 200) // 返回的状态码为200时才表示响应完全成功
        {
            std::cout << res->body; // 将返回结果打印出来
        }
    }
    else
    {
        // 发送请求失败
        return 0;
    }
    return 0;
}