# 目录说明：保存提供在线编译服务核心的模块

> 1. compiler.hpp : 编译模块
> ```
>     bool Compile(const std::string &file_name)
>     /***************************************************************************
>      * 编译函数：实现编译功能的主体。通过创建子进程，子进程调用程序替换函数来完成编译。
>      * 编译成功与否，父进程通过查看是否生成对应的可执行程序文件来判断。
>      * 输入参数：一个文件名，我们能根据该文件名拼接出要编译的文件的文件名
>      * 返回值：编译成功：true，否则：false
>      ***************************************************************************/
> ```

> 2. runner.hpp  : 运行模块
> ```
>     void SetProcLimit(int _cpu_limit, int _mem_limit)
>     /********************************************************
>      * 设置进程占用资源大小
>      *  该函数不仅仅是为了限制用户代码的运行时间和占用空间;
>      *  其更大的意义在于防止恶意用户恶意占用服务器资源。
>      ********************************************************/
> ```  
> ```
>     int Run(const std::string &file_name, int time_limit, int mem_limit)
>     /***********************************************************************
>      * 运行函数：实现编译模块创建的可执行程序的在限制的时间和空间下运行。
>      *     通过创建子进程，子进程调用程序替换函数来完成可执行程序的运行。
>      *     运行成功与否，父进程通过接收子进程返回信号来判断。
>      * 输入参数：
>      *     file_name  : 文件名，我们能根据该文件名拼接出要运行的文件的文件名。
>      *     time_limit : 该程序运行的时候，可以使用的最大运行时间上限(s)。
>      *     mem_limit  : 该程序运行的时候，可以申请的堆空间上限(KB)。
>      * 返回值：
>      *     返回值 > 0 : 程序异常，退出时收到了信号，返回值的低7位就是对应的信号。
>      *     返回值 == 0: 正常运行完毕的，结果保存到了对应的临时文件中。
>      *     返回值 < 0 : 内部错误。
>      ***********************************************************************/
> ```       
    
> 3. compile_run.hpp : 编译、运行的整合模块

> 4. compile_server.cc : 使用compile_run完成网络服务

> 5. temp文件夹：保存用户代码编译运行时产生的临时文件