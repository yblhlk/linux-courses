#pragma once

#include <signal.h>
#include <unistd.h>             //unlink()
#include <jsoncpp/json/json.h>  //引入json库（需要在开发环境中先安装json库）

#include "compiler.hpp"         //引入编译模块
#include "runner.hpp"           //引入运行模块
#include "../common/util.hpp"   //引入common目录下的工具模块（时间类，文件工具类，）
#include "../common/log.hpp"    //引入common目录下的工日志模块

//编译、运行的整合模块
//编译、运行的整合模块：正确的调用compile模块 and run模块。
//适配用户请求, 定制通信协议字段。
//形成唯一文件名。
namespace ns_compile_run
{
    using namespace ns_compiler; //展开编译模块
    using namespace ns_runner;   //展开运行模块
    using namespace ns_util;     //展开工具模块
    using namespace ns_log;      //展开日志模块

    class CompileAndRun
    {
    public:
        /*清理文件函数：清理编译运行时产生的临时文件*/
        static void RemoveTempFile(const std::string &file_name)
        {
            /*清除要编译的.cpp文件*/
            std::string _src = PathUtil::Src(file_name);
            if(FileUtil::IsFileExists(_src)) unlink(_src.c_str());//判断文件是否存在，存在就调用系统接口unlink()将其清除。
            
            /*清除编译错误文件*/
            std::string _compiler_error = PathUtil::CompilerError(file_name);
            if(FileUtil::IsFileExists(_compiler_error)) unlink(_compiler_error.c_str());

            /*清除可执行程序*/
            std::string _execute = PathUtil::Exe(file_name);
            if(FileUtil::IsFileExists(_execute)) unlink(_execute.c_str());

            /*清除标准输入文件*/
            std::string _stdin = PathUtil::Stdin(file_name);
            if(FileUtil::IsFileExists(_stdin)) unlink(_stdin.c_str());

            /*清除标准输入文件*/
            std::string _stdout = PathUtil::Stdout(file_name);
            if(FileUtil::IsFileExists(_stdout)) unlink(_stdout.c_str());

            /*清除标准错误文件*/
            std::string _stderr = PathUtil::Stderr(file_name);
            if(FileUtil::IsFileExists(_stderr)) unlink(_stderr.c_str());
        }

        /*返回状态码对应的编译运行状态信息*/
        static std::string CodeToDesc(int code, const std::string &file_name)
        {
            std::string desc;
            switch (code)
            {
            case 0:
                desc = "代码已通过编译，正在判题：";
                break;
            case -1:
                desc = "提交的代码为空，无法进行编译。";
                break;
            case -2:
                desc = "未知错误，已向服务器发送错误信息。";
                break;
            case -3:
                //编译失败，读取编译失败文件内容将其保存在输出型参数中。
                FileUtil::ReadFile(PathUtil::CompilerError(file_name), &desc, true);
                break;
            case SIGABRT: // 6
                desc = "使用内存超过范围。";
                break;
            case SIGXCPU: // 24
                desc = "运行超时。";
                break;
            case SIGFPE:  // 8
                desc = "浮点数溢出, 可能是程序存在除0错误。";
                break;
            default:
                desc = "程序运行时错误，其返回信号为: " + std::to_string(code);
                break;
            }

            return desc;
        }

        /**************************************************************************************
         * 输入一个json式的字符串，json串的内容为：
         *     code      ：用户提交的代码。
         *     input     : 用户给自己提交的代码对应的输入，后续要
         *                 设计测试用例功能时使用，现在不做处理。
         *     cpu_limit : 时间要求。
         *     mem_limit : 空间要求。
         *
         * 输出一个json式的字符串，json串的内容为：
         *     必填：
         *     status: 状态码
         *     reason: 状态码对应的编译运行状态
         *     选填：
         *     stdout: 我的程序运行完的结果
         *     stderr: 我的程序运行完的错误结果
         *
         * 参数：
         *     in_json : {"code" : "用户代码", "input":"","cpu_limit":"", "mem_limit":""}
         *     out_json: {"status" : "", "reason":"","stdout":"","stderr":"",}
         **************************************************************************************/
        static void Start(const std::string &in_json, std::string *out_json)
        {
        //1. 从Json字符串中提取数据
            //1.1 反序列化：将网络通信数据格式转为本地数据格式(这里是将一个字符串转化为Json类)
            Json::Value in_value; //创建一个Json类
            Json::Reader reader;  //提供读取服务的中间类，将字符串的内容读取到Json类中
            //1.2 通过中间类将 字符串的内容读取到 Json类中
            reader.parse(in_json, in_value);
            //1.3 从Json式的类in_value中读取数据：
            std::string code  = in_value["code"].asString();   //读取键为"code"的值，并将其转化为字符串。
            std::string input = in_value["input"].asString();  //读取键为"input"的值，并将其转化为字符串。
            int cpu_limit     = in_value["cpu_limit"].asInt(); //读取键为"cpu_limit"的值，并将其转化为整型。
            int mem_limit     = in_value["mem_limit"].asInt(); //读取键为"mem_limit"的值，并将其转化为整型。

            int status_code = 0;    //代表编译运行过程情况的信号
            int run_result  = 0;    //运行模块的返回值
            std::string file_name;  //需要内部形成的唯一文件名
            Json::Value out_value;  //保存要给用户返回内容的Josn类

            if (code.size() == 0) //代码为空
            {
                status_code = -1; //-1:表示编译运行过程中遇到代码为空情况
                goto END;
            }

        //2.创建一个唯一的文件名，没有目录没有后缀（使用毫秒级时间戳+原子性递增唯一值: 来保证唯一性）
            file_name = FileUtil::UniqFileName();

        //3.创建src文件(保存代码的cpp文件)并向src文件写入保存在json串中的用户代码
            if (!FileUtil::WriteFile(PathUtil::Src(file_name), code))
            {
                status_code = -2; //-2:表示编译运行过程中遇到未知错误
                goto END;
            }

        //4.调用编译模块
            if (!Compiler::Compile(file_name))
            {
                //编译失败
                status_code = -3; //代码编译的时候发生了错误
                goto END;
            }

        //5.调用运行模块
            run_result = Runner::Run(file_name, cpu_limit, mem_limit);
            if (run_result < 0)      //运行模块发生错误.
            {
                status_code = -2;         //-2:表示编译运行过程中遇到未知错误
            }
            else if (run_result > 0) //可执行程序运行时出错.
            {
                status_code = run_result; //>0, 设置状态码为可执行程序运行时返回的信号
            }
            else                     //运行成功.
            {
                status_code = 0;          //0, 可执行程序运行成功
            }
        END:
        //6.填写保存要给用户返回内容的Josn类并将其序列化，通过输出型参数把内容返回给用户。
            //6.1 填写Josn类
            out_value["status"] = status_code; //给键"status"赋值，让其保存要给用户返回的编译运行的状态码。
            out_value["reason"] = CodeToDesc(status_code, file_name);  //给键"reason"赋值，让其保存状态码对应的编译运行状态信息。
            if (status_code == 0)// 如果调用编译运行整个过程全部成功，表示程序运行起来了，填选填内容。
            {
                //读取标准输出文件里的内容，给Json类中的键"stdout"赋值
                std::string _stdout; 
                FileUtil::ReadFile(PathUtil::Stdout(file_name), &_stdout, true);
                out_value["stdout"] = _stdout; //给键"stdout"赋值，让其保存程序运行的结果。

                //读取标准错误文件里的内容，给Json类中的键"stderr"赋值
                std::string _stderr;
                FileUtil::ReadFile(PathUtil::Stderr(file_name), &_stderr, true);
                out_value["stderr"] = _stderr; //给键"stderr"赋值，让其保存程序运行时的错误信息。
            }
            //6.2 将Json反序列化为字符串，通过输出型参数把内容返回给用户。
            Json::StyledWriter writer;
            *out_json = writer.write(out_value);

        //7.清理编译运行过程中产生的临时文件
            RemoveTempFile(file_name);
        }
    };
}



