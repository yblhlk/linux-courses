// 使用第三方开源库cpp-httplib，处理用户在网站发起的网络请求，通过处理好的html代码给用户构建网页。
#include <iostream>
#include <signal.h> //signal():捕捉信号

#include "../common/httplib.h" //引入第三方开源库：httplib.h 来负责网络服务，完成网络请求
#include "oj_control.hpp"      //引入控制器模块
#include "oj_control4.hpp"     //引入控制器模块(竞赛版)
#include "oj_model5.hpp"
using namespace httplib;     // 展开httplib库
using namespace ns_control;  // 展开控制器模块
using namespace ns_control4; // 展开控制器模块

/*捕捉信号函数用来启动服务器*/
static Control *ctrl_ptr = nullptr;
static ns_model_login::ControlLogin *ctrl_login_ptr = nullptr;

void Recovery(int signo)
{
    ctrl_ptr->RecoveryMachine(); // 启动主机列表中所有负责编译运行服务的主机
}

int main()
{
    signal(SIGQUIT, Recovery); // 捕捉SIGQUIT信号(Ctrl+\)

    Server svr; // 创建一个客户端对象来接收并处理请求

    Control ctrl;   // 面试题
    Control4 ctrl4; // 竞赛题
    ctrl_ptr = &ctrl;

    ns_model_login::ControlLogin ctrl_login;
    ctrl_login_ptr = &ctrl_login;

    /**************************************************************************************
     * 1.用户发起/all_questions请求，申请获取所有的题目列表，使用lambda函数处理用户发起的请求。
     * lambda函数里的两个参数：
     *     const Request &req： 请求类,里面保存着用户的请求，即/question/(\d+)
     *     const Request &resq：响应类,对用户请求做出的响应
     **************************************************************************************/
    svr.Get("/all_questions", [&ctrl](const Request &req, Response &resp)
            {
                // 输出型参数，用来保存渲染好的一张包含有所有题目的网页的html代码
                std::string html;
                // control模块调用model模块和view模块渲染网页，由输出型参数返回渲染好的网页。
                ctrl.AllQuestions(&html);
                // 给用户返回渲染好的网页（会直接将网页显示在 公网IP:port/all_questions上）
                resp.set_content(html, "text/html; charset=utf-8"); });

    /**************************************************************************************
     * 1.2 用户发起/all_questions4请求，申请获取所有的竞赛题目列表，使用lambda函数处理用户发起的请求。
     * lambda函数里的两个参数：
     *     const Request &req： 请求类,里面保存着用户的请求，即/question/(\d+)
     *     const Request &resq：响应类,对用户请求做出的响应
     **************************************************************************************/
    svr.Get("/all_questions4", [&ctrl4](const Request &req, Response &resp)
            {
                // 输出型参数，用来保存渲染好的一张包含有所有题目的网页的html代码
                std::string html;
                // control模块调用model模块和view模块渲染网页，由输出型参数返回渲染好的网页。
                ctrl4.AllQuestions4(&html);
                // 给用户返回渲染好的网页（会直接将网页显示在 公网IP:port/all_questions上）
                resp.set_content(html, "text/html; charset=utf-8"); });

    /*****************************************************************************************************
     * 2.1 用户发起请求/question/(\d+)，申请获取单个题目的内容
     * 参数一：R"(/question/(\d+))"
     *     question/(\d+) : (\d+)正则匹配任意长度的数字(cpp-httplib中引入了<regex>支持了正则表达式)
     *     R"()", 原始字符串raw string,保持字符串内容的原貌，不做相关的转义(\d是转义字符，故要使用R"()")
     * 参数二：[&ctrl](const Request &req, Response &resp){}
     *     const Request &req： 请求类,里面保存着用户的请求(matches)，即/question/(\d+)和网页发来的数据(body)
     *     const Request &resq：响应类,对用户请求做出的响应
     ****************************************************************************************************/
    svr.Get(R"(/question/(\d+))", [&ctrl](const Request &req, Response &resp)
            {
                std::string number = req.matches[1];// 通过请求类，获取请求的第二部分内容: (\d+)
                std::string html ;
                // control模块调用model模块和view模块渲染网页
                ctrl.Question(number, &html);
                // 给用户返回渲染好的网页
                resp.set_content(html, "text/html; charset=utf-8"); });
    /*****************************************************************************************************
     * 2.2 用户发起请求/question4/(\d+)，申请获取竞赛列表中单个题目的内容
     * 参数一：R"(/question/(\d+))"
     *     question/(\d+) : (\d+)正则匹配任意长度的数字(cpp-httplib中引入了<regex>支持了正则表达式)
     *     R"()", 原始字符串raw string,保持字符串内容的原貌，不做相关的转义(\d是转义字符，故要使用R"()")
     * 参数二：[&ctrl](const Request &req, Response &resp){}
     *     const Request &req： 请求类,里面保存着用户的请求(matches)，即/question/(\d+)和网页发来的数据(body)
     *     const Request &resq：响应类,对用户请求做出的响应
     ****************************************************************************************************/
    svr.Get(R"(/question4/(\d+))", [&ctrl4](const Request &req, Response &resp)
            {
            std::string number = req.matches[1];// 通过请求类，获取请求的第二部分内容: (\d+)
            std::string html ;
            // control模块调用model模块和view模块渲染网页
            ctrl4.Question4(number, &html);
            // 给用户返回渲染好的网页
            resp.set_content(html, "text/html; charset=utf-8"); });

    /* 3.1 用户点击提交代码发起请求 /judge/(\d+)，申请判题*/
    svr.Post(R"(/judge/(\d+))", [&ctrl](const Request &req, Response &resp)
             {
                std::string token = req.get_header_value("Authorization");
                 std::string number = req.matches[1]; // 通过请求类，获取请求的第二部分内容: (\d+)
                 std::string result_json;
                 ctrl.Judge(number, req.body,token, &result_json); // 把网页传来Json串(req.body)给control模块下的判题模块，通过判题模块把Json串给负责编译运行服务的主机。
                 //以把包含判题结果的Json串以Json形式返回给用户
                 resp.set_content(result_json, "application/json;charset=utf-8"); });
    /* 3.2 用户点击提交代码发起请求 /judge4/(\d+)，申请判题*/
    svr.Post(R"(/judge4/(\d+))", [&ctrl4](const Request &req, Response &resp)
             {
                std::string token = req.get_header_value("Authorization");
                 std::string number = req.matches[1]; // 通过请求类，获取请求的第二部分内容: (\d+)
                 std::string result_json;
                 ctrl4.Judge4(number, req.body,token, &result_json); // 把网页传来Json串(req.body)给control模块下的判题模块，通过判题模块把Json串给负责编译运行服务的主机。
                 //以把包含判题结果的Json串以Json形式返回给用户
                 resp.set_content(result_json, "application/json;charset=utf-8"); });
    // 拓展：

    svr.Post("/islogin", [&ctrl_login](const Request &req, Response &resp)
             {
                std::cout << "进入判断是否登陆\n";
                std::string token = req.get_header_value("Authorization");
                std::string out_json;
                ctrl_login.IsLogin(token, out_json);
                std::cout << out_json << std::endl;
                resp.set_header("Content-Type", "application/json;charset=utf-8");
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    svr.Get("/login", [&ctrl_login](const Request &req, Response &resp)
            { 
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.LoginHtml(html);
                resp.set_content(html, "text/html; charset=utf-8"); });
    // 1.用户点击登录
    svr.Post("/logined", [&ctrl_login](const Request &req, Response &resp)
             {
                std::string username = req.get_param_value("username").c_str();
                std::string passwd = req.get_param_value("passwd").c_str();
                std::string token = req.get_header_value("Authorization");
                std::string out_json;
                ctrl_login.Login(username, passwd, token, &out_json);
                resp.set_header("Content-Type", "application/json;charset=utf-8");
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    // 2.用户注册功能
    svr.Get("/signin", [&ctrl_login](const Request &req, Response &resp)
            {
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.SigninHtml(html);
                resp.set_content(html, "text/html; charset=utf-8"); });

    svr.Post("/signined", [&ctrl_login](const Request &req, Response &resp)
             {
                std::string username = req.get_param_value("username").c_str();
                std::string passwd = req.get_param_value("passwd").c_str();
                std::string email = req.get_param_value("email").c_str();
                std::string out_json;
                ctrl_login.Signin(username, passwd, email, &out_json);
                resp.set_header("Content-Type", "application/json;charset=utf-8");
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    // 2.忘记密码界面显示
    svr.Get("/alteruser", [&ctrl_login](const Request &req, Response &resp)
            {
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AlterUserHtml(html);
                resp.set_content(html, "text/html; charset=utf-8"); });

    svr.Post("/getcheckid", [](const Request &req, Response &resp)
             {
                std::cout << "进入获取验证码\n";
                std::string email = req.get_param_value("email").c_str();

                std::string json;
                ns_model_login::ControlAdmin ctrl;

                ctrl.GetCheckId(email, json);
                resp.set_content(json, "application/json;charset=utf-8"); });

    svr.Post("/checkid", [](const Request &req, Response &resp)
             {
                std::string email = req.get_param_value("email").c_str();
                std::string checkid = req.get_param_value("checkid").c_str();
                std::cout << checkid << "\t" << email << std::endl;
                std::string json;
                ns_model_login::ControlAdmin ctrl;

                ctrl.CheckId(email, checkid, json);
                resp.set_content(json, "application/json;charset=utf-8"); });

    svr.Get("/alterpasswd", [](const Request &req, Response &resp)
            {
               
                std::string html;
                ns_model_login::ControlAdmin ctrl;
                ctrl.AlterPasswdHtml(html);
                resp.set_content(html, "text/html; charset=utf-8"); });

    svr.Post("/alter", [](const Request &req, Response &resp)
             {
                std::string email = req.get_param_value("email").c_str();
                std::string newpasswd = req.get_param_value("newpasswd").c_str();
                std::string alginpasswd = req.get_param_value("alginpasswd").c_str();
                
                std::string out_json;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AlterPasswd(email,newpasswd,alginpasswd,out_json);
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    svr.Post("/updateuser", [](const Request &req, Response &resp)
             {
                std::string token = req.get_header_value("Authorization");
                std::string username = req.get_param_value("username").c_str();
                std::string email = req.get_param_value("email").c_str();
                std::string userdesc = req.get_param_value("userdesc").c_str();
                
                std::string out_json;
                ns_model_login::ControlAdmin ctrl;

                ctrl.UpdateUserInfo(username,email,userdesc, token,out_json);
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    // 1.1 登陆成功进入个人用户界面
    svr.Get(R"(/login/admin_users/(\w+))", [&ctrl_login](const Request &req, Response &resp)
            { 
                vector<std::string> paths;
                ns_util::StringUtil::SplitString(req.path, &paths, "/");
                std::string adminname = paths[3];
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminUserHtml(html, adminname);
                resp.set_content(html, "text/html; charset=utf-8"); });

    // 1.1
    svr.Post("/usersinfo", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                int pagenumber = atoi(req.get_param_value("pagenumber").c_str());
                std::string out_json;
                ns_model_login::ControlAdmin ctrl;
                
                ctrl.AdminUsersInfo(pagenumber, token, out_json);
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    // 1.2 登陆成功, 发出删除请求
    svr.Post("/admin_users/delete", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                std::string delete_id_str = req.get_param_value("deleteid").c_str();

                int delete_id = atoi(delete_id_str.c_str());
                
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminDeleteUser(delete_id, token); });

    // 1.2 登陆成功, 发出删除请求
    svr.Post("/admin_question/delete", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                std::string delete_id_str = req.get_param_value("deleteid").c_str();

                int delete_id = atoi(delete_id_str.c_str());
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminDeleteQuestion(delete_id, token, html);
                resp.set_content(html, "text/html; charset=utf-8"); });

    // 1.3 登陆成功进入个人用户界面
    svr.Get(R"(/login/admin_questions/(\w+))", [&ctrl_login](const Request &req, Response &resp)
            { 
                vector<std::string> paths;
                ns_util::StringUtil::SplitString(req.path, &paths, "/");
                std::string adminname = paths[3];
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminQuestionHtml(html, adminname);
                resp.set_content(html, "text/html; charset=utf-8"); });

    // 1.3 登陆成功进入个人用户界面
    svr.Get(R"(/login/user/(\w+))", [](const Request &req, Response &resp)
            { 
                vector<std::string> paths;
                ns_util::StringUtil::SplitString(req.path, &paths, "/");
                std::string username = paths[3];
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.UserHtml(html, username);
                resp.set_content(html, "text/html; charset=utf-8"); });

    svr.Post("/userinfo", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                std::string out_json;
                ns_model_login::ControlAdmin ctrl;
                
                ctrl.UserInfo(token, out_json);
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    svr.Post("/questionsinfo", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                int pagequenumber = atoi(req.get_param_value("pagequenumber").c_str());
                std::string out_json;
                ns_model_login::ControlAdmin ctrl;
                
                ctrl.AdminQuestionsInfo(pagequenumber, token, out_json);
                resp.set_content(out_json, "application/json;charset=utf-8"); });

    // 1.3 登陆成功进入个人用户界面
    svr.Post("/admin_inputques/input", [&ctrl_login](const Request &req, Response &resp)
             { 
                std::string token = req.get_header_value("Authorization"); 
                std::string quesname = req.get_param_value("quesname").c_str();
                std::string star = req.get_param_value("star").c_str();
                std::string timelim = req.get_param_value("timelim").c_str();
                std::string spacelim = req.get_param_value("spacelim").c_str();
                std::string quesdesc = req.get_param_value("quesdesc").c_str();
                std::string quescode = req.get_param_value("quescode").c_str();
                std::string judgecode = req.get_param_value("judgecode").c_str();

                ns_model_login::Question question;
                question.title = quesname;
                question.star = star;
                question.cpu_limit = atoi(timelim.c_str());
                question.mem_limit = atoi(spacelim.c_str());
                question.desc = quesdesc;
                question.header = quescode;
                question.tail = quescode;

                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminInputques(html, token, question);
                resp.set_content(html, "text/html; charset=utf-8"); });
    // 1.3 登陆成功进入个人用户界面
    svr.Get(R"(/login/admin_input_question/(\w+))", [&ctrl_login](const Request &req, Response &resp)
            { 
                vector<std::string> paths;
                ns_util::StringUtil::SplitString(req.path, &paths, "/");
                std::string adminname = paths[3];
                
                std::string html;
                ns_model_login::ControlAdmin ctrl;

                ctrl.AdminQuestionInputHtml(html, adminname);
                resp.set_content(html, "text/html; charset=utf-8"); });

    svr.set_base_dir("./wwwroot"); // 设置网站首页
    svr.listen("0.0.0.0", 8080);
    return 0;
}