#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void Test1()
{
    vector<int> v = {1, 2, 3, 4, 5, 6};
    int max = Solution().Max(v);
    if (max == 6)
    {
        std::cout << "测试用例1通过。" << std::endl;
    }
    else
    {
        std::cout << "测试用例1....失败。" << std::endl;
    }
}

void Test2()
{
    vector<int> v = {-1, -2, -3, -4, -5, -6};
    int max = Solution().Max(v);
    if (max == -1)
    {
        std::cout << "测试用例2通过。" << std::endl;
    }
    else
    {
        std::cout << "测试用例2....失败。" << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}