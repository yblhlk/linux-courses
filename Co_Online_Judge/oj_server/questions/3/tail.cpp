#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif


void Test1()
{
    // 通过定义临时对象，来完成方法的调用
    bool ret = Solution().isPalindrome(121);
    if(ret){
        std::cout << "测试用例1通过。" << std::endl;
    }
    else{
        std::cout << "测试用例1....失败, 测试的值是: 121。"  << std::endl;
    }
}

void Test2()
{
    // 通过定义临时对象，来完成方法的调用
    bool ret = Solution().isPalindrome(-10);
    if(!ret){
        std::cout << "测试用例2通过。" << std::endl;
    }
    else{
        std::cout << "测试用例2....失败, 测试的值是: -10。"  << std::endl;
    }
}

int main()
{
    Test1();
    Test2();

    return 0;
}