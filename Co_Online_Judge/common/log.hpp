//log.hpp  : 日志模块，打印日志或向部署程序的主机打印提示
#pragma once

#include <iostream>
#include <string>   //to_string() : 将数字转化为字符串
#include "util.hpp" //引入工具模块


namespace ns_log
{
    //展开工具模块
    using namespace ns_util;

    // 日志等级
    enum
    {
        INFO,    //常规，用于打印提示信息
        DEBUG,   //调试，用于打印调试信息
        WARNING, //告警，用于打印告警信息，对运行没有影响
        ERROR,   //错误，问题影响当前模块运行
        FATAL    //严重错误，问题影响整个系统运行
    };

    //要传入的参数：日志等级、文件名(__FILE__)、行号(__LINE__)。
    //设为内联函数提高程序效率。
    //设置为输出流可使用<<在后面自由拼接内容。
    inline std::ostream &Log(const std::string &level, const std::string &file_name, int line)
    {
        //使用c++的string完成字符串的拼接。
        // 添加日志等级
        std::string message = "[";
        message += level;
        message += "]";

        // 添加报错文件名称
        message += "[";
        message += file_name;
        message += "]";

        // 添加报错行
        message += "[";
        message += std::to_string(line);
        message += "]";

        // 日志时间
        message += "[";
        message += TimeUtil::GetTime();
        message += "]";

        // cout本质：一个输出流，内部包含缓冲区。
        std::cout << message; //不要用endl，会刷新缓冲区。

        return std::cout;
    }

    /**********************************************
     * 开放式日志 ：LOG(INFO) << "message\n";
     *     #的作用是将“#”后面的宏参数进行字符串转换操作
     *     __FILE__ ：这会包含当前文件名，一个字符串常量。
     *     __LINE__ ：这会包含当前行号，一个十进制常量。
     **********************************************/
    #define LOG(level) Log(#level, __FILE__, __LINE__)
}