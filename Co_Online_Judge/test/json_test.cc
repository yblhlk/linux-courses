// 以下为第三方开源库jsoncpp库的测试代码
#include <iostream>
#include <jsoncpp/json/json.h>

int main()
{
    //1.序列化 : 将本地数据格式转为网络通信数据格式，这里是将一个Json类转化为字符串
    Json::Value root; // Value是一个Json形式的类，通过KV(键值对)保存数值。
    root["code"] = "mycode";
    root["user"] = "wyl";
    root["age"]  = "21";
    // Json::StyledWriter writer;      // 中间类，将Json形式Value转为其他数据类型。格式化。
    Json::FastWriter writer;           // 中间类，将Json形式Value转为其他数据类型。非格式化。
    std::string str = writer.write(root); // 将Value转化为std::string类型。
    std::cout << str << std::endl;

    //2.反序列化 : 将网络通信数据格式转为本地数据格式，这里是将一个字符串转化为Json类
    const std::string &in_json = R"({"age":"21","code":"mycode","user":"wyl"})";
    Json::Value in_value; //创建一个Json类
    Json::Reader reader;  //提供读取服务的中间类，将字符串的内容读取到Json类中
    // 通过中间类将 字符串的内容读取到 Json类中
    reader.parse(in_json, in_value);
    // 从Json式的类in_value中读取数据：
    int age           = in_value["age"].asInt(); //读取键为"cpu_limit"的值，并将其转化为整型。
    std::string code  = in_value["code"].asString();   //读取键为"code"的值，并将其转化为字符串。
    std::string user  = in_value["user"].asString();  //读取键为"input"的值，并将其转化为字符串。
    return 0;
}

// FastWriter(非格式化)
//{"age":"21","code":"mycode","user":"wyl"}

// StyledWriter(格式化)
//  {
//     "age" : "21",
//     "code" : "mycode",
//     "user" : "wyl"
//  }