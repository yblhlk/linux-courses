// 以下为第三方开源库ctemplate的测试代码
#include <iostream>
#include <string>
#include <ctemplate/template.h>

int main()
{
    std::string in_html = "./ctemplate_test.html";
    std::string value = "1";

    // 1.形成数据字典
    ctemplate::TemplateDictionary root("test"); //相当于：unordered_map<string, string> test;
    // 2.数据字典内容
    root.SetValue("key", value);                //相当于：test.insert({});

    // 3.获取要被渲染网页对象
    ctemplate::Template *tpl = ctemplate::Template::GetTemplate(in_html, ctemplate::DO_NOT_STRIP/*保持网页原貌*/);

    // 4.添加字典数据到网页中，并将新的网页提供输出型参数返回
    std::string out_html; //输出型参数：获取渲染后的html文件代码
    tpl->Expand(&out_html, &root);

    // 5.完成渲染后，打印html文件代码
    std::cout << out_html << std::endl;
}