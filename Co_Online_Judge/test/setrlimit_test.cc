//以下为资源限制函数setrlimit()的测试代码
#include <stdio.h>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h> //signal():信号捕捉函数


void handler(int signo)
{
    std::cout << "signo : " << signo << std::endl;
    exit(1);
}
int main()
{
    /***********************************************************
     * setrlimit(限制的资源类型, 限制结构体):用于设置进程的资源限制
     * 第一个参数 : 设置限制的资源类型。
     **** RLIMIT_CPU:设置CPU时长(用于设置程序运行时间)
     **** RLIMIT_AS:设置内存大小(用于设置程序能占用的空间大小)
     * 第二个参数 : 限制结构体，保存了限制的相关数值。
     **** rlim_max : 硬限制，软限制修改的上限。
     **** rlim_cur : 软限制，进程占用资源的上限。
     * 资源不足，导致OS中止进程，是通过信号终止的。
     **** 经测试发现：内存不足，由6号信号终止
     **** 经测试发现：时间超限，由24号信号终止
     ************************************************************/
    //捕捉信号
    for(int i = 1 ; i <= 31; ++i)
    {
        signal(i, handler);
    }
    //限制时间测试
    struct rlimit time;
    time.rlim_cur = 1;
    time.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CPU, &time);
    while(1){}

    //限制堆空间测试
    struct rlimit mem;
    mem.rlim_cur = 1024;
    mem.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_AS, &mem);
    while(1)
    {
        char* a = new char[1024*1024*2];
    }
    return 0;
}